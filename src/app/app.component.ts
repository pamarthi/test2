import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  login={
    bookname:'',
    author:'',
    price:''
  }
  table=[];
  collection=[];
  index;
  editdata:any;
  a:boolean=false
  tableDuplicate: any[];
  add(){
    this.table.push({...this.login})
    this.login={
      bookname:'',
      author:'',
      price:''
    }
    this.tableDuplicate = {...this.table}; 
    console.log(this.tableDuplicate);
  }
  delete(i){
    this.table.splice(i,1)
  }
}
