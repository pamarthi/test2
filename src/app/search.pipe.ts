import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(table: any[], searchText: any): any[] {
    if (!table) {
      return [];
    }
    if (!searchText) {
      return table;
    }
    searchText = searchText.toLowerCase();
    return table.filter(it => {
     return it.bookname.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    });
  }
}
